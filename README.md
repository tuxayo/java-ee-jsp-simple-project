# Specs
These pages as of 2016-11

http://jean-luc.massat.perso.luminy.univ-amu.fr/ens/jee/tp-jsp1.html

http://jean-luc.massat.perso.luminy.univ-amu.fr/ens/jee/tp-jsp2.html

http://jean-luc.massat.perso.luminy.univ-amu.fr/ens/jee/tp-jsp3.html

# Run Tomcat 8.X and deploy
`mvn install cargo:run`

# Redeploy
`mvn install cargo:redeploy`

# Configure Eclipse to still get auto redeploy
- go to project properties
- go to builders
- create a new builder of type "Program"
- set location: "/usr/bin/mvn" (example path of maven on a Linux machine)
- Working Directory → Browse Workspace → Ok
  This sets working dir at project root
- Arguments: "install cargo:redeploy"
- go to Build Options
- check "During auto builds"

Now you have an IDE independent way to run and redeploy the app without having to manually setup and install Tomcat in your IDE. But you still get the convenience of auto redeploy.

# Commit messages
As they are only used as checkpoints, their quality is really low.
They are often copy pastes from the specs so I can quickly find which
is the last implemented thing.
