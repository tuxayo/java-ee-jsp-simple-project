<%@ taglib prefix="mestags" uri="http://table.tags.example.org"%>

<%@ page language="java"
    contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" %>

<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
    <title>Test table with SimpleTabSupport</title>
    <style>
      table, td {
          border: 1px solid black;
          border-spacing: 0px;
      }
    </style>
  </head>
<body>
  <h1>Test table with SimpleTabSupport</h1>

  <mestags:tableau centrer="oui">
    <mestags:ligne>première ligne</mestags:ligne>
    <mestags:ligne>deuxième ligne</mestags:ligne>
    <mestags:ligne>troisième ligne</mestags:ligne>
    <mestags:ligne>quatrième ligne</mestags:ligne>
  </mestags:tableau>

</body>
</html>
