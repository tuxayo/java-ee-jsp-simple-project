<%@ page contentType="text/html; charset=ISO-8859-1"%>
<%@ taglib prefix="mytest" uri="http://www.tomcat-demo.com/testing"%>

<mytest:header>This is a test</mytest:header>

<mytest:header headerSize="1">I'm still testing</mytest:header>

<mytest:header headerSize="2">The test continues</mytest:header>

<mytest:header headerSize="3">The test is over</mytest:header>
