<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java"
    contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>Title</title>
</head>
<body>
  <p>Hello.</p>
  <p>
    <%= new java.util.Date() %>
  </p>
  <c:url var="person" value="/person.jsp" />
  <p><a href="${person}">./person.jsp</a></p>
  <p><a href="./person.jspx">./person.jspx</a></p>

  <c:url var="edition" value="/edition.jsp" />
  <p><a href="${edition}">./edition.jsp</a></p>

  <c:url var="lister" value="/lister.jsp" />
  <p><a href="${lister}">./lister.jsp</a></p>

  <p><a href="./test-jstl.jsp?foo=bars&key=value&question=oui">./test-jstl.jsp?foo=bars&key=value&question=oui</a></p>
  <p><a href="./test-custom-tags.jsp">./test-custom-tags.jsp</a></p>
  <p><a href="./test-simple-tag-support/example-tag-with-body.jsp">./test-simple-tag-support/example-tag-with-body.jsp</a></p>
  <p><a href="./test-simple-tag-support/table.jsp">./test-simple-tag-support/table.jsp</a></p>
  <p><a href="./test-JSPX.jspx">./test-JSPX.jspx</a></p>
</body>
</html>
