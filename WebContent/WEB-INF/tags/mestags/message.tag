<%@ tag language="java" pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%@ attribute name="type" required="true" description="Type du message" %>

<%@ attribute name="textes" type="java.util.List" required="false" %>

<p>Votre contenu (type ${type}) : <jsp:doBody /></p>

<c:if test="${textes != null}">
  <p>Vos textes :</p>
  <ul>
  <c:forEach var="texte" items="${textes}">
    <li><c:out value="${texte}"/></li>
  </c:forEach>
  </ul>
</c:if>
