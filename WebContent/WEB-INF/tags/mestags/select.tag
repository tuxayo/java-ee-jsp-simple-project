<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%@ attribute name="name" required="true" %>
<%@ attribute name="value" type="org.example.business.PersonStatus" required="true" %>
<%@ attribute name="options" type="java.util.HashMap" required="true" %>
<%@ attribute name="label" required="false" %>

<c:if test="${label != null}">
  <label>${label} </label>
</c:if>
    <select name="status">
      <option value=""></option>
      <c:forEach var="option" items="${options.keySet()}">
        <c:set var="optionStr" value="${options[option]}" />
        <c:set var="optionShouldBeSelected" value="${option == value}" />
        <option value="${optionStr}" ${optionShouldBeSelected ? "selected" : ""}>
          ${optionStr}
        </option>
      </c:forEach>
    </select>
