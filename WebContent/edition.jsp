<%@ page import="org.example.business.PersonStatus"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page import="org.example.beans.PersonFormErrors"%>
<%@ taglib prefix="mestags" tagdir="/WEB-INF/tags/mestags" %>
<%@ page language="java"
    contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>Edit</title>
</head>
<body>

<% // form errors handling
    PersonFormErrors formErrors = (PersonFormErrors) session.getAttribute("formErrors");
    session.removeAttribute("formErrors");
    if (formErrors != null) {
        pageContext.setAttribute("emailErrorMessage", formErrors.getEmailErrorMessage());
        pageContext.setAttribute("lastNameErrorMessage", formErrors.getLastNameErrorMessage());
    }
%>

<jsp:useBean id="statuses" scope="page" class="java.util.HashMap" >
<%
  statuses.put(PersonStatus.Student, PersonStatus.Student.toString());
  statuses.put(PersonStatus.Teacher, PersonStatus.Teacher.toString());
  statuses.put(PersonStatus.Exterior, PersonStatus.Exterior.toString());
%>
</jsp:useBean>

<c:set var="person" value="${sessionScope['person']}" />

<form action="./edition" method="POST">
  <label>First name : </label>
    <input type="text" name="firstName" value="${person.firstName}"/>
    <br>

  <label>Last name : </label>
    <input type="text" name="lastName" value="${person.lastName}"/>
    <c:choose>
      <c:when test="${pageScope['lastNameErrorMessage']!= null}">
        <font color="red">
          <c:out value="${pageScope['lastNameErrorMessage']}"/>
        </font>
      </c:when>
    </c:choose>
    <br>

  <label>Birth date : </label>
    <input type="text" name="birthDate" value="${person.birthDate}"/>
    <br>

  <label>Email : </label>
    <input type="text" name="email" value="${person.email}"/>
    <c:choose>
      <c:when test="${pageScope['emailErrorMessage']!= null}">
        <font color="red">
          <c:out value="${pageScope['emailErrorMessage']}"/>
        </font>
      </c:when>
    </c:choose>
    <br>

   <mestags:select
    name="status"
    value="${person.status}"
    options="${statuses}"
    label="Status :"
    />

  <br>
  <input type="submit" name="submitButton" value="Submit"/>

</form>

</body>
</html>