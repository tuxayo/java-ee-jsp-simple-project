<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="java.util.Collection"%>
<%@ page import="org.example.business.PersonManager"%>
<%@ page import="java.util.List"%>
<%@ page import="org.example.beans.Person"%>
<%@ page language="java"
    contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>Persons list</title>
</head>

<%
    PersonManager personManager = PersonManager.getInstance(getServletContext());
    Collection<Person> persons = personManager.findAll();
    pageContext.setAttribute("persons", persons);
%>

<body>
  <a href="edition">Ajouter une personne</a>
  <p>List of persisted persons</p>

  <table>
    <tr>
      <th>Id</th>
      <th>First name</th>
      <th>Last name</th>
      <th>Birth date</th>
      <th>Email</th>
      <th>Status</th>
      <th>Actions</th>
    </tr>
    <c:set var="persons" value="${pageScope[\"persons\"]}" />
    <c:forEach var="person" items="${persons}">
      <c:set var="id" value="${person.id}" />
      <tr>
        <td><c:out value="${id}"/></td>
        <td>
          <a href="edition?id=<c:out value="${id}"/>">
            <c:out value="${person.firstName}"/>
          </a>
        </td>
        <td><c:out value="${person.lastName}"/></td>
        <td><c:out value="${person.birthDate}"/></td>
        <td><c:out value="${person.email}"/></td>
        <td><c:out value="${person.status}"/></td>
        <td>
          <a href="delete?id=<c:out value="${id}"/>">Delete</a>
        </td>
      </tr>
    </c:forEach>
  </table>
</body>
</html>
