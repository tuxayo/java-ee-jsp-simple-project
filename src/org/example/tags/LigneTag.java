package org.example.tags;

import java.io.IOException;
import java.io.StringWriter;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.PageContext;
import javax.servlet.jsp.tagext.JspFragment;
import javax.servlet.jsp.tagext.SimpleTagSupport;

public class LigneTag extends SimpleTagSupport {

    public void doTag() throws JspException, IOException {
        PageContext pageContext = (PageContext) getJspContext();
        JspWriter out = pageContext.getOut();

        StringWriter sw = new StringWriter();
        sw.append("<tr><td>");

        JspFragment body = getJspBody();
        body.invoke(sw);

        sw.append("</td></tr>");
        out.println(sw.toString());
    }
}
