package org.example.tags;

import java.io.IOException;
import java.io.StringWriter;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.PageContext;
import javax.servlet.jsp.tagext.JspFragment;
import javax.servlet.jsp.tagext.SimpleTagSupport;

public class TableauTag extends SimpleTagSupport {
    private String centrer;

    public void setCentrer(String centrer) {
        this.centrer = centrer;
    }

    public void doTag() throws JspException, IOException {
        PageContext pageContext = (PageContext) getJspContext();
        JspWriter out = pageContext.getOut();
        StringWriter sw = new StringWriter();

        String tableStyle = "";
        if(centrer.equals("oui")) {
            tableStyle = " style=\"margin: auto;\"";
        }
        sw.append("<table" + tableStyle + ">");

        JspFragment body = getJspBody();
        body.invoke(sw);

        sw.append("</table>");
        out.println(sw.toString());
    }
}
