package org.example.servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.example.beans.Person;
import org.example.beans.PersonFormErrors;
import org.example.business.PersonManager;
import org.example.business.PersonStatus;
import org.example.business.PersonValidationException;

@WebServlet(
    name = "edition",
    urlPatterns = "/edition"
    )
public class Edition extends HttpServlet {
    private static final long serialVersionUID = 7720854814685995247L;

    public void doGet(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {
        String idParam = request.getParameter("id");
        int id = 0;
        boolean isAndEdition = idParam != null; // otherwise it's a creation
        if (isAndEdition) {
            id = Integer.parseInt(idParam);
        }
        PersonManager personManager = PersonManager.getInstance(getServletContext());
        Person personToEdit = personManager.find(id);
        HttpSession session = request.getSession();
        session.setAttribute("person", personToEdit);
        renderForm(request, response);
    }

    public void doPost(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {
        HttpSession session = request.getSession();
        Person person = (Person) session.getAttribute("person");
        if (person == null) {
            person = new Person();
        }
        person.setFirstName(request.getParameter("firstName"));
        person.setLastName(request.getParameter("lastName"));
        person.setBirthDate(request.getParameter("birthDate"));
        person.setEmail(request.getParameter("email"));
        PersonStatus status = PersonStatus.fromString(request.getParameter("status"));
        person.setStatus(status);
        session.setAttribute("person", person);

        PersonManager personManager = PersonManager.getInstance(getServletContext());
        try {
            personManager.check(person);
            personManager.save(person);
            // keep it in session so the form will be pre-filed with the last
            // Person
            person = person.clone();
            session.setAttribute("person", person);
            getServletContext().setAttribute("personManager", personManager);
            response.sendRedirect("./person.jsp");
        } catch (PersonValidationException e) {
            PersonFormErrors formErrors = new PersonFormErrors();
            if (e.isEmailInvalid()) {
                formErrors.setEmailErrorMessage("invalid");
            }
            if (e.isNameMissing()) {
                formErrors.setLastNameErrorMessage("missing");
            }
            session.setAttribute("formErrors", formErrors);
            response.sendRedirect("./edition.jsp");
        }
    }

    private void renderForm(HttpServletRequest request,
                            HttpServletResponse response)
                                    throws ServletException, IOException {
        request.getRequestDispatcher("./edition.jsp")
            .forward(request, response);
    }
}
