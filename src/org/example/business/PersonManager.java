package org.example.business;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ThreadLocalRandom;

import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.servlet.ServletContext;

import org.example.beans.Person;

public class PersonManager {

    final private Map<Integer, Person> persons;

    @SuppressWarnings("serial")
    public PersonManager() {
        Person p0 = new Person(1, "Jane", "Doe", "1/1/1970", "jane@example.org", PersonStatus.Teacher);
        Person p1 = new Person(2, "Sylvester", "Staline", "12/12/1912", "Sylvester@example.org", PersonStatus.Exterior);
        persons = new HashMap<Integer, Person>() {{
            put(p0.getId(), p0); // literal hashmap initialization
            put(p1.getId(), p1);
        }};
    }

    public static PersonManager getInstance(ServletContext context) {
        PersonManager personManager = (PersonManager) context.getAttribute("personManager");
        if (personManager == null) {
            personManager = new PersonManager();
        }
        return personManager;
    }
    
    public Collection<Person> findAll() {
        return persons.values();
    }

    public Person find(int id) {
        return persons.get(id);
    }

    public void save(Person p) {
        if (p.getId() == 0) {
            int id = ThreadLocalRandom.current().nextInt(0, Integer.MAX_VALUE);
            p.setId(id);
        }
        persons.put(p.getId(), p);
    }

    public void check(Person p) throws IllegalArgumentException {
        boolean isNameMissing = (p.getLastName() == "" || p.getLastName() == null);
        boolean isEmailInvalid = !isValidEmailAddress(p.getEmail());
        if (isEmailInvalid || isNameMissing) {
            PersonValidationException e = new PersonValidationException();
            e.setEmailInvalid(isEmailInvalid);
            e.setNameMissing(isNameMissing);
            throw e;
        }
    }

    /**
     * credits for this function: <a href="http://stackoverflow.com/questions/624581/what-is-the-best-java-email-address-validation-method/5931718#5931718">Source</a>
     */
    private static boolean isValidEmailAddress(String email) {
        boolean result = true;
        try {
            InternetAddress emailAddr = new InternetAddress(email);
            emailAddr.validate();
        } catch (AddressException ex) {
            result = false;
        }
        return result;
     }

    public void delete(int id) {
        persons.remove(id);
    }

}
