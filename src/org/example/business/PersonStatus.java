package org.example.business;

public enum PersonStatus {
     Student, Teacher, Exterior;

    public static PersonStatus fromString(String statusStr) {
        switch (statusStr) {
        case "Student":
            return Student;
        case "Exterior":
            return Exterior;
        case "Teacher":
            return Teacher;
        default:
            throw new IllegalArgumentException(statusStr + " is not a valid status");
        }
    }
}
