package org.example.business;

@SuppressWarnings("serial")
public class PersonValidationException extends IllegalArgumentException {
    private boolean isEmailInvalid;
    private boolean isNameMissing;

    public boolean isEmailInvalid() {
        return isEmailInvalid;
    }

    public void setEmailInvalid(boolean isEmailInvalid) {
        this.isEmailInvalid = isEmailInvalid;
    }

    public boolean isNameMissing() {
        return isNameMissing;
    }

    public void setNameMissing(boolean isNameMissing) {
        this.isNameMissing = isNameMissing;
    }
}
