package org.example.beans;

public class PersonFormErrors {
    private String emailErrorMessage = null;
    private String lastNameErrorMessage = null;

    public String getEmailErrorMessage() {
        return emailErrorMessage;
    }

    public void setEmailErrorMessage(String emailErrorMessage) {
        this.emailErrorMessage = emailErrorMessage;
    }

    public String getLastNameErrorMessage() {
        return lastNameErrorMessage;
    }

    public void setLastNameErrorMessage(String lastNameErrorMessage) {
        this.lastNameErrorMessage = lastNameErrorMessage;
    }
}