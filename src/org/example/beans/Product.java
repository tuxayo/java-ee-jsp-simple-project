package org.example.beans;

public class Product {
    String name;
    String price;
    String desc;

    public Product() {};

    public String getName()            { return name; }
    public String getPrice()           { return price; }
    public String getDesc()            { return desc; }
    public void setName(String name)   { this.name = name; }
    public void setPrice(String price) { this.price = price; }
    public void setDesc(String desc)   { this.desc = desc; }
}
